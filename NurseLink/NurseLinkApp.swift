//
//  NurseLinkApp.swift
//  NurseLink
//
//  Created by Lusine Gasparyan on 06.05.22.
//

import SwiftUI

@main
struct NurseLinkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
